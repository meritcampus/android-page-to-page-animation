package com.example.pagetopagenavigationanimation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity implements OnClickListener {
ImageView imageView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button btnLeft = (Button) findViewById(R.id.left);
		Button btnRight = (Button) findViewById(R.id.right);
		Button btnTop = (Button) findViewById(R.id.top);
		Button btndown = (Button) findViewById(R.id.down);
		Button btnBlink = (Button) findViewById(R.id.blink);
		imageView =(ImageView) findViewById(R.id.imageView);
		btnLeft.setOnClickListener(this);
		btnRight.setOnClickListener(this);
		btnTop.setOnClickListener(this);
		btndown.setOnClickListener(this);
		btnBlink.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent intent = null;
		switch (v.getId()) {
		// left to right animation
		case R.id.left:
			intent = new Intent(getApplicationContext(), AnimationExample.class);
			startActivity(intent);
			overridePendingTransition(R.anim.left_to_right_in_animation,
					R.anim.left_to_right_out_animation);
			break;
			// right to left animation
		case R.id.right:
			intent = new Intent(getApplicationContext(), AnimationExample.class);
			startActivity(intent);

			overridePendingTransition(R.anim.right_to_left_in_animation,
					R.anim.right_to_left_out_animation);
			break;
		case R.id.top:
			intent = new Intent(getApplicationContext(), AnimationExample.class);
			startActivity(intent);
			overridePendingTransition(R.anim.top_in,
					R.anim.top_out);
			break;
		case R.id.down:
			intent = new Intent(getApplicationContext(), AnimationExample.class);
			startActivity(intent);
			overridePendingTransition(R.anim.bottom_in,
					R.anim.bottom_out);
			break;
		case R.id.blink:
			Animation animation=AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink);
			imageView.setAnimation(animation);
			break;
		default:
			break;
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
